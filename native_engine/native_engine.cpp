/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "native_engine.h"

#if !defined(WINDOWS_PLATFORM) && !defined(MAC_PLATFORM) && !defined(IOS_PLATFORM)
#include <sys/epoll.h>
#endif
#include <uv.h>

#include "utils/log.h"

NativeEngine::NativeEngine(void* jsEngine) : jsEngine_(jsEngine) {}

NativeEngine::~NativeEngine()
{
    std::lock_guard<std::mutex> insLock(instanceDataLock_);
    FinalizerInstanceData();
}

NativeScopeManager* NativeEngine::GetScopeManager()
{
    return nativeEngineImpl_->GetScopeManager();
}

NativeReferenceManager* NativeEngine::GetReferenceManager()
{
    return nativeEngineImpl_->GetReferenceManager();
}

NativeModuleManager* NativeEngine::GetModuleManager()
{
    return nativeEngineImpl_->GetModuleManager();
}

NativeCallbackScopeManager* NativeEngine::GetCallbackScopeManager()
{
    return nativeEngineImpl_->GetCallbackScopeManager();
}

uv_loop_t* NativeEngine::GetUVLoop() const
{
    return nativeEngineImpl_->GetUVLoop();
}

pthread_t NativeEngine::GetTid() const
{
    return nativeEngineImpl_->GetTid();
}

void NativeEngine::Loop(LoopMode mode, bool needSync)
{
    nativeEngineImpl_->Loop(mode, needSync);
}

NativeAsyncWork* NativeEngine::CreateAsyncWork(NativeValue* asyncResource, NativeValue* asyncResourceName,
    NativeAsyncExecuteCallback execute, NativeAsyncCompleteCallback complete, void* data)
{
    return nativeEngineImpl_->CreateAsyncWork(this, asyncResource, asyncResourceName, execute, complete, data);
}

NativeAsyncWork* NativeEngine::CreateAsyncWork(const std::string &asyncResourceName, NativeAsyncExecuteCallback execute,
                                               NativeAsyncCompleteCallback complete,
                                               void* data)
{
    return nativeEngineImpl_->CreateAsyncWork(this, asyncResourceName, execute, complete, data);
}

NativeSafeAsyncWork* NativeEngine::CreateSafeAsyncWork(NativeValue* func, NativeValue* asyncResource,
    NativeValue* asyncResourceName, size_t maxQueueSize, size_t threadCount, void* finalizeData,
    NativeFinalize finalizeCallback, void* context, NativeThreadSafeFunctionCallJs callJsCallback)
{
    return nativeEngineImpl_->CreateSafeAsyncWork(this, func, asyncResource, asyncResourceName, maxQueueSize,
        threadCount, finalizeData, finalizeCallback, context, callJsCallback);
}

void NativeEngine::InitAsyncWork(NativeAsyncExecuteCallback execute,
                                 NativeAsyncCompleteCallback complete,
                                 void* data)
{
    nativeEngineImpl_->InitAsyncWork(this, execute, complete, data);
}

bool NativeEngine::SendAsyncWork(void* data)
{
    return nativeEngineImpl_->SendAsyncWork(data);
}

void NativeEngine::CloseAsyncWork()
{
    nativeEngineImpl_->CloseAsyncWork();
}

NativeErrorExtendedInfo* NativeEngine::GetLastError()
{
    return nativeEngineImpl_->GetLastError();
}

void NativeEngine::SetLastError(int errorCode, uint32_t engineErrorCode, void* engineReserved)
{
    nativeEngineImpl_->SetLastError(errorCode, engineErrorCode, engineReserved);
}

void NativeEngine::ClearLastError()
{
    nativeEngineImpl_->ClearLastError();
}

bool NativeEngine::IsExceptionPending() const
{
    return nativeEngineImpl_->IsExceptionPending();
}

NativeValue* NativeEngine::GetAndClearLastException()
{
    return nativeEngineImpl_->GetAndClearLastException();
}

void NativeEngine::EncodeToUtf8(NativeValue* nativeValue,
                                char* buffer,
                                int32_t* written,
                                size_t bufferSize,
                                int32_t* nchars)
{
    nativeEngineImpl_->EncodeToUtf8(nativeValue, buffer, written, bufferSize, nchars);
}

void NativeEngine::EncodeToChinese(NativeValue* nativeValue, std::string& buffer, const std::string& encoding)
{
    nativeEngineImpl_->EncodeToChinese(nativeValue, buffer, encoding);
}

#if !defined(WINDOWS_PLATFORM) && !defined(MAC_PLATFORM)
void NativeEngine::CheckUVLoop()
{
#ifndef IOS_PLATFORM
    nativeEngineImpl_->CheckUVLoop();
#endif
}

void NativeEngine::CancelCheckUVLoop()
{
#ifndef IOS_PLATFORM
    nativeEngineImpl_->CancelCheckUVLoop();
#endif
}
#endif

void NativeEngine::SetPostTask(PostTask postTask)
{
    HILOG_INFO("SetPostTask in");
    nativeEngineImpl_->SetPostTask(postTask);
}

void NativeEngine::TriggerPostTask()
{
    nativeEngineImpl_->TriggerPostTask();
}

void* NativeEngine::GetJsEngine()
{
    return nativeEngineImpl_->GetJsEngine();
}

// register init worker func
void NativeEngine::SetInitWorkerFunc(InitWorkerFunc func)
{
    initWorkerFunc_ = func;
}
void NativeEngine::SetGetAssetFunc(GetAssetFunc func)
{
    getAssetFunc_ = func;
}
void NativeEngine::SetOffWorkerFunc(OffWorkerFunc func)
{
    offWorkerFunc_ = func;
}
void NativeEngine::SetWorkerAsyncWorkFunc(NativeAsyncExecuteCallback executeCallback,
                                          NativeAsyncCompleteCallback completeCallback)
{
    nativeAsyncExecuteCallback_ = executeCallback;
    nativeAsyncCompleteCallback_ = completeCallback;
}
// call init worker func
bool NativeEngine::CallInitWorkerFunc(NativeEngine* engine)
{
    if (initWorkerFunc_ != nullptr) {
        initWorkerFunc_(engine);
        return true;
    }
    return false;
}
bool NativeEngine::CallGetAssetFunc(const std::string& uri, std::vector<uint8_t>& content, std::string& ami)
{
    if (getAssetFunc_ != nullptr) {
        getAssetFunc_(uri, content, ami);
        return true;
    }
    return false;
}
bool NativeEngine::CallOffWorkerFunc(NativeEngine* engine)
{
    if (offWorkerFunc_ != nullptr) {
        offWorkerFunc_(engine);
        return true;
    }
    return false;
}

bool NativeEngine::CallWorkerAsyncWorkFunc(NativeEngine* engine)
{
    if (nativeAsyncExecuteCallback_ != nullptr && nativeAsyncCompleteCallback_ != nullptr) {
        engine->InitAsyncWork(nativeAsyncExecuteCallback_, nativeAsyncCompleteCallback_, nullptr);
        return true;
    }
    return false;
}

void NativeEngine::AddCleanupHook(CleanupCallback fun, void* arg)
{
    nativeEngineImpl_->AddCleanupHook(fun, arg);
}

void NativeEngine::RemoveCleanupHook(CleanupCallback fun, void* arg)
{
    nativeEngineImpl_->RemoveCleanupHook(fun, arg);
}

void NativeEngine::RunCleanup()
{
    nativeEngineImpl_->RunCleanup();
}

void NativeEngine::CleanupHandles()
{
    nativeEngineImpl_->CleanupHandles();
}

void NativeEngine::IncreaseWaitingRequestCounter()
{
    nativeEngineImpl_->IncreaseWaitingRequestCounter();
}

void NativeEngine::DecreaseWaitingRequestCounter()
{
    nativeEngineImpl_->DecreaseWaitingRequestCounter();
}

void NativeEngine::RegisterWorkerFunction(const NativeEngine* engine)
{
    if (engine == nullptr) {
        return;
    }
    SetInitWorkerFunc(engine->initWorkerFunc_);
    SetGetAssetFunc(engine->getAssetFunc_);
    SetOffWorkerFunc(engine->offWorkerFunc_);
    SetWorkerAsyncWorkFunc(engine->nativeAsyncExecuteCallback_, engine->nativeAsyncCompleteCallback_);
}

NativeValue* NativeEngine::RunScript(const char* path)
{
    std::vector<uint8_t> scriptContent;
    std::string pathStr(path);
    std::string ami;
    if (!CallGetAssetFunc(pathStr, scriptContent, ami)) {
        HILOG_ERROR("Get asset error");
        return nullptr;
    }
    HILOG_INFO("asset size is %{public}zu", scriptContent.size());
    return RunActor(scriptContent, ami.c_str());
}

NativeEngineInterface* NativeEngine::GetNativeEngineImpl()
{
    return nativeEngineImpl_;
}

void NativeEngine::SetInstanceData(void* data, NativeFinalize finalize_cb, void* hint)
{
    HILOG_INFO("NativeEngineWraper::%{public}s, start.", __func__);
    std::lock_guard<std::mutex> insLock(instanceDataLock_);
    FinalizerInstanceData();
    instanceDataInfo_.engine = this;
    instanceDataInfo_.callback = finalize_cb;
    instanceDataInfo_.nativeObject = data;
    instanceDataInfo_.hint = hint;
}

void NativeEngine::GetInstanceData(void** data)
{
    HILOG_INFO("NativeEngineWraper::%{public}s, start.", __func__);
    std::lock_guard<std::mutex> insLock(instanceDataLock_);
    if (data) {
        *data = instanceDataInfo_.nativeObject;
    }
}

void NativeEngine::FinalizerInstanceData(void)
{
    if (instanceDataInfo_.engine != nullptr && instanceDataInfo_.callback != nullptr) {
        instanceDataInfo_.callback(instanceDataInfo_.engine, instanceDataInfo_.nativeObject, instanceDataInfo_.hint);
    }
    instanceDataInfo_.engine = nullptr;
    instanceDataInfo_.callback = nullptr;
    instanceDataInfo_.nativeObject = nullptr;
    instanceDataInfo_.hint = nullptr;
}

const char* NativeEngine::GetModuleFileName()
{
    HILOG_INFO("%{public}s, start.", __func__);
    NativeModuleManager* moduleManager = nativeEngineImpl_->GetModuleManager();
    HILOG_INFO("NativeEngineWraper::GetFileName GetModuleManager");
    if (moduleManager != nullptr) {
        const char* moduleFileName = moduleManager->GetModuleFileName(moduleName_.c_str(), isAppModule_);
        HILOG_INFO("NativeEngineWraper::GetFileName end filename:%{public}s", moduleFileName);
        return moduleFileName;
    }
    return nullptr;
}

void NativeEngine::SetModuleFileName(std::string &moduleName)
{
    moduleName_ = moduleName;
}

void NativeEngine::DeleteEngine()
{
    if (nativeEngineImpl_) {
        delete nativeEngineImpl_;
        nativeEngineImpl_ = nullptr;
    }
}
