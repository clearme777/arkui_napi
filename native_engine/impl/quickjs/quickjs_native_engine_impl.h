/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FOUNDATION_ACE_NAPI_NATIVE_ENGINE_IMPL_QUICKJS_QUICKJS_NATIVE_ENGINE_IMPL_H
#define FOUNDATION_ACE_NAPI_NATIVE_ENGINE_IMPL_QUICKJS_QUICKJS_NATIVE_ENGINE_IMPL_H

#include "native_engine/native_engine_interface.h"
#include "quickjs_headers.h"

#include "quickjs_native_engine.h"

class SerializeData {
public:
    SerializeData(size_t size, uint8_t* data) : dataSize_(size), value_(data) {}
    ~SerializeData() = default;

    uint8_t* GetData() const
    {
        return value_.get();
    }
    size_t GetSize() const
    {
        return dataSize_;
    }

private:
    struct Deleter {
        void operator()(uint8_t* ptr) const
        {
            free(ptr);
        }
    };

    size_t dataSize_;
    std::unique_ptr<uint8_t, Deleter> value_;
};

class QuickJSNativeEngineImpl : public NativeEngineInterface {
public:
    QuickJSNativeEngineImpl(JSRuntime* runtime, JSContext* contex, NativeEngine* engine, void* jsEngine);
    virtual ~QuickJSNativeEngineImpl();

    JSRuntime* GetRuntime();
    JSContext* GetContext();

    virtual void Loop(LoopMode mode, bool needSync = false) override;

    virtual NativeValue* GetGlobal(NativeEngine* engine) override;
    virtual NativeValue* CreateNull(NativeEngine* engine) override;
    virtual NativeValue* CreateUndefined(NativeEngine* engine) override;
    virtual NativeValue* CreateBoolean(NativeEngine* engine, bool value) override;
    virtual NativeValue* CreateNumber(NativeEngine* engine, int32_t value) override;
    virtual NativeValue* CreateNumber(NativeEngine* engine, uint32_t value) override;
    virtual NativeValue* CreateNumber(NativeEngine* engine, int64_t value) override;
    virtual NativeValue* CreateNumber(NativeEngine* engine, double value) override;
    virtual NativeValue* CreateBigInt(NativeEngine* engine, int64_t value) override;
    virtual NativeValue* CreateBigInt(NativeEngine* engine, uint64_t value) override;
    virtual NativeValue* CreateString(NativeEngine* engine, const char* value, size_t length) override;
    virtual NativeValue* CreateString16(NativeEngine* engine, const char16_t* value, size_t length) override;
    virtual NativeValue* CreateSymbol(NativeEngine* engine, NativeValue* value) override;
    virtual NativeValue* CreateExternal(
        NativeEngine* engine, void* value, NativeFinalize callback, void* hint) override;

    virtual NativeValue* CreateObject(NativeEngine* engine) override;
    virtual NativeValue* CreateNativeBindingObject(NativeEngine* engine, void* detach, void* attach) override;
    virtual NativeValue* CreateFunction(
        NativeEngine* engine, const char* name, size_t length, NativeCallback cb, void* value) override;
    virtual NativeValue* CreateArray(NativeEngine* engine, size_t length) override;

    virtual NativeValue* CreateArrayBuffer(NativeEngine* engine, void** value, size_t length) override;
    virtual NativeValue* CreateArrayBufferExternal(
        NativeEngine* engine, void* value, size_t length, NativeFinalize cb, void* hint) override;
    virtual NativeValue* CreateBuffer(NativeEngine* engine, void** value, size_t length) override;
    virtual NativeValue* CreateBufferCopy(
        NativeEngine* engine, void** value, size_t length, const void* data) override;
    virtual NativeValue* CreateBufferExternal(
        NativeEngine* engine, void* value, size_t length, NativeFinalize cb, void* hint) override;
    virtual NativeValue* CreateTypedArray(
        NativeEngine* engine, NativeTypedArrayType type, NativeValue* value, size_t length, size_t offset) override;
    virtual NativeValue* CreateDataView(
        NativeEngine* engine, NativeValue* value, size_t length, size_t offset) override;
    virtual NativeValue* CreatePromise(NativeEngine* engine, NativeDeferred** deferred) override;
    virtual void SetPromiseRejectCallback(
        NativeEngine* engine, NativeReference* rejectCallbackRef, NativeReference* checkCallbackRef) override;
    virtual NativeValue* CreateError(NativeEngine* engine, NativeValue* code, NativeValue* Message) override;
    virtual NativeValue* CreateInstance(
        NativeEngine* engine, NativeValue* constructor, NativeValue* const *argv, size_t argc) override;

    virtual NativeReference* CreateReference(NativeEngine* engine, NativeValue* value, uint32_t initialRefcount,
        NativeFinalize callback = nullptr, void* data = nullptr, void* hint = nullptr) override;
    virtual NativeValue* CallFunction(NativeEngine* engine, NativeValue* thisVar, NativeValue* function,
        NativeValue* const *argv, size_t argc) override;

    virtual NativeValue* DefineClass(NativeEngine* engine, const char* name, NativeCallback callback, void* data,
        const NativePropertyDescriptor* properties, size_t length) override;

    virtual NativeValue* RunScript(NativeEngine* engine, NativeValue* script) override;
    NativeValue* RunScriptPath(NativeEngine* engine, const char* path) override
    {
        return nullptr;
    }
    virtual NativeValue* RunBufferScript(NativeEngine* engine, std::vector<uint8_t>& buffer) override;
    virtual NativeValue* RunActor(NativeEngine* engine, std::vector<uint8_t>& buffer, const char* descriptor) override;

    void SetPackagePath(const std::string& packagePath);

    virtual bool Throw(NativeValue* error) override;
    virtual bool Throw(NativeEngine* engine, NativeErrorType type, const char* code, const char* message) override;

    virtual void* CreateRuntime(NativeEngine* engine) override;
    bool CheckTransferList(JSValue transferList);
    bool DetachTransferList(JSValue transferList);
    virtual NativeValue* Serialize(NativeEngine* context, NativeValue* value, NativeValue* transfer) override;
    virtual NativeValue* Deserialize(NativeEngine* engine, NativeEngine* context, NativeValue* recorder) override;
    virtual void DeleteSerializationData(NativeValue* value) const override;
    virtual ExceptionInfo* GetExceptionForWorker() const override;
    virtual NativeValue* LoadModule(NativeEngine* engine, NativeValue* str, const std::string& fileName) override;

    static NativeValue* JSValueToNativeValue(QuickJSNativeEngine* engine, JSValue value);
    virtual NativeValue* ValueToNativeValue(NativeEngine* engine, JSValueWrapper& value) override;
    JSValue GetModuleFromName(QuickJSNativeEngine* engine, const std::string& moduleName, bool isAppModule,
        const std::string& id, const std::string& param, const std::string& instanceName, void** instance);
    JSValue LoadModuleByName(
        QuickJSNativeEngine* engine, const std::string& moduleName, bool isAppModule, const std::string& param,
        const std::string& instanceName, void* instance);

    virtual NativeValue* CreateDate(NativeEngine* engine, double time) override;
    virtual NativeValue* CreateBigWords(
        NativeEngine* engine, int sign_bit, size_t word_count, const uint64_t* words) override;
    virtual bool TriggerFatalException(NativeValue* error) override;
    virtual bool AdjustExternalMemory(int64_t ChangeInBytes, int64_t* AdjustedValue) override;

    void StartCpuProfiler(const std::string& fileName = "") override {}
    void StopCpuProfiler() override {}

    void ResumeVM() override {}
    bool SuspendVM() override
    {
        return false;
    }
    bool IsSuspended() override
    {
        return false;
    }
    bool CheckSafepoint() override
    {
        return false;
    }

    void DumpHeapSnapshot(const std::string& path, bool isVmMode = true,
        DumpFormat dumpFormat = DumpFormat::JSON) override {}
    void DumpHeapSnapshotExt(bool isVmMode = true, DumpFormat dumpFormat = DumpFormat::JSON,
        bool isPrivate = false) override {};
    bool BuildNativeAndJsStackTrace(std::string& stackTraceStr) override
    {
        return false;
    }
    bool BuildJsStackTrace(std::string& stackTraceStr) override
    {
        return false;
    }
    bool StartHeapTracking(double timeInterval, bool isVmMode = true) override
    {
        return false;
    }
    bool StopHeapTracking(const std::string& filePath) override
    {
        return false;
    }

    void PrintStatisticResult() override {}
    void StartRuntimeStat() override {}
    void StopRuntimeStat() override {}
    size_t GetArrayBufferSize() override
    {
        return 0;
    }
    size_t GetHeapTotalSize() override
    {
        return 0;
    }
    size_t GetHeapUsedSize() override
    {
        return 0;
    }

    void RegisterUncaughtExceptionHandler(UncaughtExceptionCallback callback) override {}
    void HandleUncaughtException(NativeEngine* engine) override {}
private:
    static NativeEngine* CreateRuntimeFunc(NativeEngine* engine, void* jsEngine);

    JSRuntime* runtime_;
    JSContext* context_;
};

#endif /* FOUNDATION_ACE_NAPI_NATIVE_ENGINE_IMPL_QUICKJS_QUICKJS_NATIVE_ENGINE_IMPL_H */
